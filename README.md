## Getting started ##

```
$ git clone https://CptHeartless@bitbucket.org/CptHeartless/backoffice.git
$ cd backoffice
$ npm install
```

## Usage ##

- Use ``npm run server`` to spin up the dev server
- Use ``npm run build`` to compile source

## Note ##

- Read [ES6 styleguide by airbnb](https://github.com/airbnb/javascript)
- Read [Angular styleguide by johnpapa](https://github.com/johnpapa/angular-styleguide/tree/master/a1)
- Read [lodash docs](lodash.com/docs)
- Read [Jade reference](http://jade-lang.com/reference/)