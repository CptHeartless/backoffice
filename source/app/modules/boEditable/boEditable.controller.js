function boEditableController($element, $timeout, $scope) {
  const vm = this;

  vm.showValue = false;
  vm.$viewValue = $element.find('span');
  vm.$size = angular.element($element[0].querySelector('.bo-editable__size'));
  vm.value = vm.initialValue;

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function resizeInput(text = vm.value ? vm.value : vm.placeholder) {
    vm.$size.text(text);
    const maxWidth = $element.parent()[0].offsetWidth;
    const sizeWidth = vm.$size[0].offsetWidth;

    $element.css('max-width', `${maxWidth}px`);
    $element.find('input').css('width', `${sizeWidth}px`);
  }

  function hideInput() {
    vm.showValue = false;
  }

  function toggleState() {
    vm.showValue = !vm.showValue;
    if (vm.showValue) {
      $timeout(() => {
        const $input = $element.find('input').length
          ? $element.find('input') : $element.find('textarea');
        $input[0].focus();
      });
    }
    resizeInput();
  }

  function decline() {
    hideInput();
    vm.value = vm.initialValue;
    vm.onChange();
    $apply();
  }

  function accept() {
    if (!vm.value.length) {
      decline();
    } else {
      hideInput();
      vm.value = vm.value.trim();
      vm.initialValue = vm.value;
    }
    $apply();
  }

  vm.accept = accept;
  vm.decline = decline;
  vm.hideInput = hideInput;
  vm.resizeInput = resizeInput;
  vm.toggleState = toggleState;
}

boEditableController.$inject = ['$element', '$timeout', '$scope'];

module.exports = boEditableController;
