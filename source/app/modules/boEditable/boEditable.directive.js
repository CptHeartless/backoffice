const boEditableTemplate = require('./boEditable.view.pug');
const boEditableController = require('./boEditable.controller');

function boEditableDirective() {
  const directive = {
    template: boEditableTemplate,
    transclude: true,
    restrict: 'EA',
    replace: true,
    scope: {
      initialValue: '=',
      placeholder: '@',
      maxLength: '@',
      multiline: '@',
      onChange: '&',
    },
    controller: boEditableController,
    controllerAs: 'boEditableVm',
    bindToController: true,
  };

  return directive;
}

module.exports = boEditableDirective;
