const modalDirective = require('./modal.directive');

angular.module('bo.modal', [])
  .directive('modal', modalDirective);
