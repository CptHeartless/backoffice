const boCarouselDirective = require('./boCarousel.directive');

angular.module('bo.carousel', [])
  .directive('boCarousel', boCarouselDirective);
