function scenariosService() {
  this.scenarios = [
    { id: 0, name: 'Base' },
    { id: 1, name: 'Vanila' },
    { id: 2, name: 'Burning Crusade' },
    { id: 3, name: 'Legion' },
  ];

  function getAll() {
    return this.scenarios;
  }

  function getById(id) {
    return this.scenarios.filter(scenario => scenario.id === id)[0];
  }

  this.getAll = getAll;
  this.getById = getById;
}

module.exports = scenariosService;
