function usersFilter(items, from, status) {
  if (!from) return items;
  const result = [];
  _.forEach(items, (n) => {
    let isRight = false;

    if (~n.name.toUpperCase().indexOf(from.toUpperCase())) {
      isRight = true;
    } else if (~n.login.toUpperCase().indexOf(from.toUpperCase())) {
      isRight = true;
    } else if (~status[n.status].toUpperCase().indexOf(from.toUpperCase())) {
      isRight = true;
    } else {
      const creationDate = new Date(n.creationDate);
      const dd = creationDate.getDate();
      let mm = creationDate.getMonth() + 1;
      const yyyy = creationDate.getFullYear();
      if (mm < 10) mm = `0${mm}`;
      const stringCreationDate = `${dd}.${mm}.${yyyy}`;
      if (~stringCreationDate.indexOf(from)) isRight = true;
    }
    if (isRight) result.push(n);
  });
  return result;
}

function wrapUsersFilter() {
  return usersFilter;
}

module.exports = wrapUsersFilter;
