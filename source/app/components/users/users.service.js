function usersService() {
  this.users = [
    {
      id: 0,
      name: 'Steve Jobs',
      login: 'Jobs55',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 1,
      name: 'Steve Jobs Jr',
      login: 'Jobs1955',
      status: 0,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 2,
      name: 'Вол\'джин',
      login: 'VolJin',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 3,
      name: 'Оргрим Молот Рока',
      login: 'Orgrimm',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 4,
      name: 'Тралл',
      login: 'Thrall',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 5,
      name: 'Дуротан',
      login: 'dur07an',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 6,
      name: 'Бейн Кровавое Копыто',
      login: 'Baine',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 7,
      name: 'Кил\'Джеден',
      master: true,
      login: 'Kiljaeden',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 8,
      name: 'Громмаш Адский Крик',
      login: 'grommash',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 9,
      name: 'Иллидан Ярость Бури',
      login: '1ll1dan',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 10,
      name: 'Малфурион Ярость Бури',
      login: 'malfurion',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 11,
      name: 'Варианн Ринн',
      login: 'VarianWrynn',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 12,
      name: 'Тиранда Шелест Ветра',
      login: 'Tyrande',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 13,
      name: 'Генн Седогрив',
      login: 'GennGrey',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 14,
      name: 'Бранн Бронзобород',
      login: 'Brann666',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 15,
      name: 'Данат Троллебой',
      login: 'DonnutTrollbane',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 16, name: 'Медив',
      login: 'Mediv',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 17,
      name: 'Саргерас',
      login: 'sargeras',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 18,
      name: 'Азшара',
      login: 'ashara',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 19,
      name: 'Аман\'Тул',
      login: 'amanthul',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 20,
      name: 'Норганнон',
      master: true,
      login: 'norgannon',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 21,
      name: 'Чернорук',
      login: 'BackHand',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 22,
      name: 'Гул\'дан',
      login: 'GulDan',
      status: 2,
      image: '',
      creationDate: 1467110736823,
    },
    {
      id: 666,
      name: 'David Brin',
      login: 'DavidBrin',
      status: 2,
      image: '',
      creationDate: 1467720077935,
      master: true,
    },
  ];

  this.status = ['Администратор', 'Разработчик', 'Игрок'];
  this.permissions = [];

  function getById(id) {
    return this.users.filter(user => user.id === id)[0];
  }

  function getByIds(ids) {
    if (ids.length === 0) return [];
    const users = [];
    ids.forEach((id) => {
      users.push(this.getById(id));
    });
    return users;
  }

  function getAll() {
    return { users: this.users, status: this.status };
  }

  this.getById = getById;
  this.getByIds = getByIds;
  this.getAll = getAll;
}

module.exports = usersService;
