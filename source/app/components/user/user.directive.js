const userTemplate = require('./user.view.pug');
const userController = require('./user.controller.js');

function userDirective() {
  const directive = {
    template: userTemplate,
    restrict: 'EA',
    replace: true,
    controller: userController,
    scope: { userId: '=' },
    controllerAs: 'userVm',
    bindToController: true,
  };
  return directive;
}

module.exports = userDirective;
