function userController(users, $scope) {
  const vm = this;
  const allUsers = users.getAll();

  vm.newUser = {
    status: 2,
  };

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function setStatus(statusId) {
    if (vm.user) {
      vm.user.status = statusId;
    } else {
      vm.newUser.status = statusId;
    }
    $apply();
  }

  function uploadImage(element, userId = vm.userId) {
    const file = element.files[0];
    vm.users[userId].image = file.name;
    $scope.$apply();
  }

  vm.status = allUsers.status;
  vm.setStatus = setStatus;
  vm.uploadImage = uploadImage;

  function onUserIdChange(userId) {
    vm.user = users.getById(userId);
  }

  const userIdUnwatch = $scope.$watch('userVm.userId', onUserIdChange);

  function onDestroy() {
    userIdUnwatch();
  }

  $scope.$on('$destroy', onDestroy);
}

userController.$inject = ['users', '$scope'];

module.exports = userController;
