const gamesService = require('./games.service');
const gamesDirective = require('./games.directive');

angular.module('backoffice')
  .service('games', gamesService)
  .directive('games', gamesDirective);
