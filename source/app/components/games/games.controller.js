function gamesController(games, users, $filter, $state, $scope, scenarios) {
  const vm = this;
  vm.searchText = '';
  vm.sortProperty = '';
  vm.currentModal = '';
  vm.createGameName = '';
  vm.gameToDelete = null;
  vm.isSortReverse = true;
  vm.games = [];
  vm.gamesRaw = games.getAll();

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function fillGames() {
    vm.games = [];
    vm.gamesRaw.forEach((game) => {
      const creator = users.getById(game.creatorId);
      const masters = users.getByIds(game.mastersIds);
      const creationDateFormatted = $filter('date')(game.creationDate, 'd.MM.yyyy');
      const scenario = scenarios.getById(game.scenarioId);
      const mastersNames = masters.reduce((names, master) => {
        names.push(master.name);
        return names;
      }, []).join(', ');
      const gameData = Object.assign({ creator, masters, mastersNames,
        creationDateFormatted, scenario }, game);
      vm.games.push(gameData);
    });
    $apply();
  }

  fillGames();

  function setSortProperty(property) {
    vm.isSortReverse = vm.sortProperty === property ? !vm.isSortReverse : true;
    vm.sortProperty = property;
  }

  function showModal(modal) {
    vm.currentModal = modal;
  }

  function isCurrentModal(modal) {
    return vm.currentModal === modal;
  }

  function hideModal() {
    vm.currentModal = null;
  }

  function createGame(name = vm.createGameName) {
    if (name === '') return;
    games.create(name).then((gameId) => {
      $state.go('game', { gameId });
    });
  }

  function isSortProperty(property) {
    return {
      _down: vm.sortProperty === property && vm.isSortReverse,
      _up: vm.sortProperty === property && !vm.isSortReverse,
    };
  }

  function setGameToDelete(game) {
    vm.gameToDelete = game;
  }

  function deleteGame() {
    if (!vm.gameToDelete) return;
    vm.hideModal();
    games.deleteById(vm.gameToDelete.id).then(() => {
      fillGames();
    });
  }

  vm.deleteGame = deleteGame;
  vm.setGameToDelete = setGameToDelete;
  vm.createGame = createGame;
  vm.hideModal = hideModal;
  vm.showModal = showModal;
  vm.isCurrentModal = isCurrentModal;
  vm.isSortProperty = isSortProperty;
  vm.setSortProperty = setSortProperty;
}

gamesController.$inject = ['games', 'users', '$filter', '$state', '$scope', 'scenarios'];

module.exports = gamesController;
