function gameService(users) {
  this.games = [
    {
      id: 0,
      name: 'World of Warcraft',
      scenarioId: 2,
      maxMembers: 46,
      period: 3,
      membersIds: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
        19, 20, 21, 22, 666, 1],
      mastersIds: [7, 20, 666],
      teamsIds: [1, 2, 3],
      creatorId: 666,
      interface: true,
      finished: true,
      description: 'Игра для тестирования возможностей симулятора',
      creationDate: 1467110671672,
      steps: [
        { period: 1, name: 'Полугодие 1 2017 год' },
        { period: 2, name: 'Полугодие 2 2017 год' },
        { period: 3, name: 'Полугодие 1 2018 год' },
        { period: 4, name: 'Полугодие 2 2028 год' },
        { period: 5, name: 'Полугодие 1 2019 год' },
        { period: 6, name: 'Полугодие 2 2019 год' },
        { period: 7, name: 'Полугодие 1 2020 год' },
        { period: 8, name: 'Полугодие 2 2020 год' },
      ],
    },
    {
      id: 1,
      name: 'Skolkovo',
      scenarioId: 0,
      maxMembers: 46,
      period: 1,
      membersIds: [0, 1, 2, 10, 11, 21, 20],
      mastersIds: [20],
      teamsIds: [4],
      creatorId: 666,
      interface: false,
      description: 'Игра для тестирования возможностей симулятора',
      creationDate: 1467789524781,
      steps: [
        { period: 1, name: 'Полугодие 1 2017 год' },
        { period: 2, name: 'Полугодие 2 2017 год' },
        { period: 3, name: 'Полугодие 1 2018 год' },
        { period: 4, name: 'Полугодие 2 2028 год' },
        { period: 5, name: 'Полугодие 1 2019 год' },
        { period: 6, name: 'Полугодие 2 2019 год' },
        { period: 7, name: 'Полугодие 1 2020 год' },
        { period: 8, name: 'Полугодие 2 2020 год' },
      ],
    },
  ];

  function getAll() {
    return this.games;
  }

  function getById(gameId) {
    return this.games.filter(game => game.id === gameId)[0];
  }

  function addTeam(game, teamId) {
    if (!this.games.includes(game)) return Promise.reject(new Error('Game does not exist'));
    if (this.games.includes(teamId)) return Promise.reject(new Error('Team is exist'));
    game.teamsIds.push(teamId);
    return Promise.resolve(game.id);
  }

  function addTeamById(gameId, teamId) {
    const game = this.getById(gameId);
    return this.addTeam(game, teamId);
  }

  function deleteById(id) {
    const game = this.getById(id);
    return new Promise((resolve) => {
      this.games.splice(this.games.indexOf(game), 1);
      resolve(true);
    });
  }

  function create(name) {
    const id = _.last(this.games).id + 1;
    this.games.push({
      name,
      id,
      scenarioId: null,
      maxMembers: 46,
      period: 0,
      membersIds: [],
      mastersIds: [],
      teamsIds: [],
      creatorId: 666,
      interface: false,
      description: 'Игра для тестирования возможностей симулятора',
      creationDate: Date.now(),
      steps: [
        { period: 1, name: 'Полугодие 1 2017 год' },
        { period: 2, name: 'Полугодие 2 2017 год' },
        { period: 3, name: 'Полугодие 1 2018 год' },
        { period: 4, name: 'Полугодие 2 2028 год' },
        { period: 5, name: 'Полугодие 1 2019 год' },
        { period: 6, name: 'Полугодие 2 2019 год' },
        { period: 7, name: 'Полугодие 1 2020 год' },
        { period: 8, name: 'Полугодие 2 2020 год' },
      ],
    });
    return new Promise((resolve) => {
      resolve(id);
    });
  }

  function addMember(game, userId) {
    return new Promise((resolve, reject) => {
      if (game.membersIds.includes(userId)) return reject(new Error('Member is exist'));
      game.membersIds.push(userId);
      const user = users.getById(userId);
      if (user.master) game.mastersIds.push(userId);
      return resolve(true);
    });
  }

  function removeMember(game, userId) {
    return new Promise((resolve, reject) => {
      if (!game.membersIds.includes(userId)) return reject(new Error('Member is not exist'));
      game.membersIds.splice(game.membersIds.indexOf(userId), 1);
      if (game.mastersIds.includes(userId)) {
        game.mastersIds.splice(game.mastersIds.indexOf(userId), 1);
      }
      return resolve(true);
    });
  }

  function changeScenario(game, scenarioId) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(new Error('Game not found'));
      Object.assign(game, { scenarioId });
      return resolve(game);
    });
  }

  function fakeCalc(game, callback) {
    let progress = 0;
    return new Promise((resolve) => {
      const progressInterval = setInterval(() => {
        if (progress >= 100) {
          clearInterval(progressInterval);
          return resolve(true);
        }
        let changed = Math.round(Math.random() * 25 + 1);
        if (changed > 100 - progress) changed = 100 - progress;
        progress += changed;
        return callback(progress);
      }, 700);
    });
  }

  function calc(game, callback) {
    if (!this.games.includes(game)) return false;

    const nextPeriod = game.period + 1;

    function onProgressChange(progress) {
      callback(progress);
    }

    return fakeCalc(game, onProgressChange)
      .then(() => {
        if (nextPeriod >= game.steps.length) {
          game.steps.push({ period: nextPeriod + 1, name: 'Новый период' });
        }
        Object.assign(game, { period: nextPeriod });
        return true;
      });
  }

  function toggleInterface(game) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(false);
      Object.assign(game, { interface: !game.interface });
      return resolve(true);
    });
  }

  function finish(game) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(false);
      Object.assign(game, { finished: true });
      return resolve(true);
    });
  }

  function resume(game) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(false);
      Object.assign(game, { finished: false });
      return resolve(true);
    });
  }

  function changeName(game, name) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(false);
      Object.assign(game, { name });
      return resolve(true);
    });
  }

  function changeDescription(game, description) {
    return new Promise((resolve, reject) => {
      if (!this.games.includes(game)) return reject(false);
      Object.assign(game, { description });
      return resolve(true);
    });
  }

  this.changeDescription = changeDescription;
  this.changeName = changeName;
  this.finish = finish;
  this.resume = resume;
  this.toggleInterface = toggleInterface;
  this.calc = calc;
  this.changeScenario = changeScenario;
  this.addMember = addMember;
  this.addTeam = addTeam;
  this.removeMember = removeMember;
  this.addTeamById = addTeamById;
  this.deleteById = deleteById;
  this.create = create;
  this.getById = getById;
  this.getAll = getAll;
}

gameService.$inject = ['users'];

module.exports = gameService;
