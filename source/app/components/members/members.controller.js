function membersController($stateParams, $scope) {
  const vm = this;
  vm.currentModal = '';
  vm.gameId = $stateParams.gameId;

  function showModal(modal) {
    vm.currentModal = modal;
  }

  function isCurrentModal(modal) {
    return vm.currentModal === modal;
  }

  function hideModal() {
    vm.currentModal = null;
  }

  function removeMemberFromTeam(user) {
    $scope.$emit('game:member::remove', { userId: user.id });
  }

  vm.removeMemberFromTeam = removeMemberFromTeam;
  vm.showModal = showModal;
  vm.isCurrentModal = isCurrentModal;
  vm.hideModal = hideModal;
}

membersController.$inject = ['$stateParams', '$scope'];

module.exports = membersController;
