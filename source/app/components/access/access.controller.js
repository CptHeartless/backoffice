function accessController(users, $filter) {
  const vm = this;
  const allUsers = users.getAll();

  vm.sortProperty = '';
  vm.isSortReverse = true;
  vm.users = allUsers.users;
  vm.status = allUsers.status;
  vm.searchText = undefined;
  vm.userToDelete = undefined;
  vm.currentModal = undefined;

  function showModal(modal) {
    vm.currentModal = modal;
  }

  function isCurrentModal(modal) {
    return vm.currentModal === modal;
  }

  function hideModal() {
    vm.currentModal = null;
  }

  function showUserInfo(userId) {
    vm.showModal('userInfo');
    vm.userId = Number(userId);
  }

  function setUserToDelete(user) {
    vm.userToDelete = user;
    vm.showModal('deleteUser');
  }

  function deleteUser(user) {
    vm.users.splice(vm.users.indexOf(user), 1);
  }

  function sortUsers() {
    vm.users = $filter('usersFilter')(allUsers.users, vm.searchText, vm.status);
  }

  function saveUserData(userId = vm.userId, user = vm.users[userId]) {
    if (_.isNumber(userId)) {
      return alert(`Данные пользователя ${user.name}, сохранены`);
    }
    return alert('Создан новый пользователь');
  }

  function setSortProperty(property) {
    vm.isSortReverse = vm.sortProperty === property ? !vm.isSortReverse : true;
    vm.sortProperty = property;
  }

  function isSortProperty(property) {
    return {
      _down: vm.sortProperty === property && vm.isSortReverse,
      _up: vm.sortProperty === property && !vm.isSortReverse,
    };
  }

  vm.isSortProperty = isSortProperty;
  vm.setUserToDelete = setUserToDelete;
  vm.showUserInfo = showUserInfo;
  vm.showModal = showModal;
  vm.hideModal = hideModal;
  vm.isCurrentModal = isCurrentModal;
  vm.deleteUser = deleteUser;
  vm.setSortProperty = setSortProperty;
  vm.sortUsers = sortUsers;
  vm.saveUserData = saveUserData;
}

accessController.$inject = ['users', '$filter'];

module.exports = accessController;
