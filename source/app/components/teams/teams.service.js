function teamsService(games) {
  this.teams = [
    { id: 1, name: 'Орда', membersIds: [21, 2, 3, 4, 5, 6, 8], image: 'http://fin.d.modeus.info/import/fin/images/logos/1.png' },
    { id: 2, name: 'Альянс', membersIds: [10, 11, 12, 13, 14, 15], image: 'http://fin.d.modeus.info/import/fin/images/logos/2.png' },
    { id: 3, name: 'Пылающий Легион', membersIds: [1, 16, 17, 9, 7], image: 'http://fin.d.modeus.info/import/fin/images/logos/3.png' },
    { id: 4, name: 'Масоны', membersIds: [0, 1, 2, 21], image: 'http://fin.d.modeus.info/import/fin/images/logos/4.png' },
  ];

  function getAll() {
    return this.teams;
  }

  function getById(id) {
    return this.teams.filter(team => team.id === id)[0];
  }

  function getByIds(ids) {
    const teams = [];
    ids.forEach((teamId) => {
      teams.push(this.getById(teamId));
    });

    return teams;
  }

  function create(gameId) {
    const game = games.getById(gameId);
    const id = _.last(this.teams).id + 1;
    const name = `Команда ${game.teamsIds.length + 1}`;
    const image = `http://fin.d.modeus.info/import/fin/images/logos/${game.teamsIds.length + 1}.png`;
    const team = { id, name, image, membersIds: [] };
    this.teams.push(team);
    return games.addTeam(game, id).then(() => team);
  }

  function deleteById(teamId, game) {
    const team = this.getById(teamId);
    this.teams.splice(this.teams.indexOf(team), 1);
    game.teamsIds.splice(game.teamsIds.indexOf(teamId), 1);
    return Promise.resolve(true);
  }

  function removeMember(team, memberId) {
    return new Promise((resolve, reject) => {
      if (!this.teams.includes(team)) return reject(new Error('Teams is not exist'));
      if (!team.membersIds.includes(memberId)) return reject(new Error('Member is not exist'));
      team.membersIds.splice(team.membersIds.indexOf(memberId), 1);
      return resolve(true);
    });
  }

  function addMemberById(teamId, memberId) {
    const team = this.getById(teamId);
    return new Promise((resolve, reject) => {
      if (_.isUndefined(team)) return reject(new Error('Team is not exist'));
      if (team.membersIds.includes(memberId)) return reject(new Error('Member is exist'));
      team.membersIds.push(memberId);
      return resolve(true);
    });
  }

  this.addMemberById = addMemberById;
  this.removeMember = removeMember;
  this.deleteById = deleteById;
  this.create = create;
  this.getByIds = getByIds;
  this.getById = getById;
  this.getAll = getAll;
}

teamsService.$inject = ['games'];

module.exports = teamsService;
