function teamsController(teams, games, users, $stateParams, $scope) {
  const vm = this;
  vm.currentActiveTeam = null;
  vm.teams = [];
  vm.unassignedMembers = [];
  vm.game = games.getById($stateParams.gameId);
  vm.deletedTeam = null;
  vm.teamsRaw = [];

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function fillTeam(team) {
    const members = users.getByIds(team.membersIds);
    const teamData = Object.assign({ members }, team);
    vm.teamsRaw.push(team);
    vm.teams.push(teamData);
  }

  function fillTeams() {
    vm.teams = [];
    vm.teamsRaw = teams.getByIds(vm.game.teamsIds);
    vm.teamsRaw.forEach((team) => {
      fillTeam(team);
    });
  }

  function onMembersChange() {
    const members = users.getByIds(vm.game.membersIds);
    vm.unassignedMembers = members
      .filter(member => !vm.teams.some(team => team.members.includes(member)));
    $apply();
  }

  function getMemberTeam(memberId) {
    return vm.teamsRaw
      .filter(teamData => teamData.membersIds.includes(memberId))[0];
  }

  function removeMemberFromTeam(event, { userId, team = false }) {
    let currentTeam = {};
    if (!team) {
      currentTeam = getMemberTeam(userId);
    } else {
      currentTeam = team;
    }
    if (!currentTeam) return;
    teams.removeMember(currentTeam, userId).then(() => {
      vm.teams.filter(teamData => teamData.id === currentTeam.id)[0]
      .members.forEach((member, index, members) => {
        if (member.id === userId) {
          members.splice(index, 1);
        }
      });
      onMembersChange();
    });
  }

  function setCurrentActiveTeam(id) {
    vm.currentActiveTeam = id;
  }

  function createTeam(gameId = $stateParams.gameId) {
    teams.create(gameId).then((team) => {
      fillTeam(team);
      $apply();
    });
  }

  function deleteTeam(team, game = vm.game) {
    vm.deletedTeam = team.id;
    teams.deleteById(team.id, game).then(() => {
      vm.teams.splice(vm.teams.indexOf(team), 1);
      vm.unassignedMembers.push(...team.members);
      vm.teamsRaw = teams.getByIds(vm.game.teamsIds);
      vm.deletedTeam = null;
    });
  }

  function addMemberToTeam(team, member) {
    if (_.isNull(member)) return;
    const currentTeam = getMemberTeam(member.id);
    if (!!currentTeam && currentTeam.id === team.id) return;
    removeMemberFromTeam(null, { userId: member.id, team: currentTeam });
    teams.addMemberById(team.id, member.id).then(() => {
      team.members.push(member);
      onMembersChange();
      $apply();
    });
  }

  fillTeams();

  const memberLengthUnwatch = $scope.$watch('vm.game.membersIds.length', onMembersChange);
  const removeMemberUnwatch = $scope.$on('game:member::remove', removeMemberFromTeam);

  function onDestroy() {
    memberLengthUnwatch();
    removeMemberUnwatch();
  }

  $scope.$on('$destroy', onDestroy);

  vm.addMemberToTeam = addMemberToTeam;
  vm.deleteTeam = deleteTeam;
  vm.createTeam = createTeam;
  vm.setCurrentActiveTeam = setCurrentActiveTeam;
}

teamsController.$inject = ['teams', 'games', 'users', '$stateParams', '$scope'];

module.exports = teamsController;
