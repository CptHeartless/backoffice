function addMembersController(users, games, $scope) {
  const vm = this;
  vm.game = games.getById(vm.gameId);
  vm.usersRaw = users.getAll().users;
  vm.sortProperty = '';
  vm.searchText = '';
  vm.showFilter = 0;
  vm.showFilters = ['всех', 'в игре', 'не в игре'];
  vm.users = [];

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function fillUsers() {
    vm.users = [];
    const membersIds = vm.game.membersIds;
    vm.usersRaw.forEach((userData) => {
      const inGame = membersIds.includes(userData.id);
      const user = Object.assign({ inGame }, userData);
      vm.users.push(user);
    });
    $apply();
  }

  function addUserToGame(user) {
    games.addMember(vm.game, user.id).then(() => {
      fillUsers();
    });
  }

  function removeUserFromGame(user) {
    games.removeMember(vm.game, user.id).then(() => {
      fillUsers();
      return user.id;
    }).then((userId) => {
      $scope.$emit('game:member::remove', { userId });
    });
  }

  function toggleUserInGame(user) {
    if (user.inGame) {
      removeUserFromGame(user);
    } else {
      addUserToGame(user);
    }
  }

  function isSortProperty(property) {
    return vm.sortProperty === property;
  }

  function setSortProperty(property) {
    vm.sortProperty = property;
  }

  function toggleShowFilter() {
    if (vm.showFilter >= vm.showFilters.length - 1) {
      vm.showFilter = 0;
    } else {
      vm.showFilter += 1;
    }
  }

  fillUsers();

  vm.toggleShowFilter = toggleShowFilter;
  vm.removeUserFromGame = removeUserFromGame;
  vm.addUserToGame = addUserToGame;
  vm.toggleUserInGame = toggleUserInGame;
  vm.isSortProperty = isSortProperty;
  vm.setSortProperty = setSortProperty;
}

addMembersController.$inject = ['users', 'games', '$scope'];

module.exports = addMembersController;
