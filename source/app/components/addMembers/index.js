const addMembersDirective = require('./addMembers.directive');

angular.module('backoffice')
  .directive('addMembers', addMembersDirective);
