function gameController(teams, $stateParams, games, users, $state, scenarios, $scope) {
  const vm = this;
  vm.gameRaw = games.getById($stateParams.gameId);
  if (!vm.gameRaw) $state.go('backoffice');

  let creator = {};
  vm.scenarios = scenarios.getAll();
  vm.teams = [];
  vm.startProgress = 0;
  vm.isCalculating = false;
  const scenario = scenarios.getById(vm.gameRaw.scenarioId);

  if (vm.gameRaw) creator = users.getById(vm.gameRaw.creatorId);

  function generateLifecycle() {
    const lifecycle = [];
    const lifecycleTypes = [
      { id: 'past', name: 'Прошлые' },
      { id: 'present', name: 'Текущий' },
      { id: 'present-future', name: 'Следующий' },
      { id: 'future', name: 'Будущие' },
    ];
    let prevStepType = null;
    vm.gameRaw.steps.forEach((step) => {
      let stepType = 1;
      let progress = 0;
      if (step.period < vm.gameRaw.period) {
        progress = 100;
        stepType = 0;
      }
      if (step.period > vm.gameRaw.period) stepType = 3;
      if (step.period === vm.gameRaw.period + 1 && !vm.gameRaw.finished) stepType = 2;
      const { period, name } = step;
      lifecycle.push({ period, name, progress });
      _.last(lifecycle).type = lifecycleTypes[stepType];
      if (prevStepType !== stepType) {
        _.last(lifecycle).first = true;
        prevStepType = stepType;
      }
    });

    return lifecycle;
  }

  vm.game = Object.assign({ creator, scenario, lifecycle: generateLifecycle() }, vm.gameRaw);
  vm.currentSetup = '';

  function $apply() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply();
    }
  }

  function updateLifecycle() {
    vm.game.lifecycle = generateLifecycle();
  }

  function showSetup(setup) {
    vm.currentSetup = vm.currentSetup === setup ? '' : setup;
  }

  function changeScenario(newScenario) {
    games.changeScenario(vm.gameRaw, newScenario.id).then(() => {
      vm.game.scenario = newScenario;
      vm.game.scenarioId = newScenario.id;
      $apply();
    });
  }

  function hasScenario() {
    return !!vm.game.scenario;
  }

  function isFormedTeams() {
    return vm.teams.length && vm.teams.some(team => team.membersIds.length);
  }

  function canCalc() {
    return hasScenario() && isFormedTeams();
  }

  function startCalc() {
    if (!vm.canCalc || vm.isCalculating) return;
    vm.isCalculating = true;
    function onProgressChange(progress) {
      const currrentStep = vm.game.lifecycle.find(step => step.period === vm.game.period);
      if (currrentStep) {
        currrentStep.progress = progress;
      } else {
        vm.startProgress = progress;
      }
      $apply();
    }

    games.calc(vm.gameRaw, this::onProgressChange).then(() => {
      vm.game.period = vm.gameRaw.period;
      updateLifecycle();
      vm.isCalculating = false;
      $apply();
    });
  }

  function toggleInterface() {
    games.toggleInterface(vm.gameRaw).then(() => {
      vm.game.interface = vm.gameRaw.interface;
    });
  }

  function finishGame() {
    games.finish(vm.gameRaw).then(() => {
      vm.game.finished = true;
      updateLifecycle();
    });
  }

  function resumeGame() {
    games.resume(vm.gameRaw).then(() => {
      vm.game.finished = false;
      updateLifecycle();
    });
  }

  function changeName(name) {
    games.changeName(vm.gameRaw, name).then(() => {
      vm.game.name = name;
    });
  }

  function changeDescription(description) {
    games.changeDescription(vm.gameRaw, description).then(() => {
      vm.game.description = description;
    });
  }

  function onTeamsLengthChange() {
    vm.teams = teams.getByIds(vm.gameRaw.teamsIds);
  }

  const teamsLengthUnwatch = $scope.$watch('vm.gameRaw.teamsIds.length', onTeamsLengthChange);

  vm.changeDescription = changeDescription;
  vm.changeName = changeName;
  vm.resumeGame = resumeGame;
  vm.finishGame = finishGame;
  vm.toggleInterface = toggleInterface;
  vm.startCalc = startCalc;
  vm.canCalc = canCalc;
  vm.isFormedTeams = isFormedTeams;
  vm.hasScenario = hasScenario;
  vm.showSetup = showSetup;
  vm.changeScenario = changeScenario;
}

gameController.$inject = ['teams', '$stateParams', 'games', 'users', '$state',
  'scenarios', '$scope'];

module.exports = gameController;
